/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { BackstageTestModule } from '../../../test.module';
import { EdicionDialogComponent } from '../../../../../../main/webapp/app/entities/edicion/edicion-dialog.component';
import { EdicionService } from '../../../../../../main/webapp/app/entities/edicion/edicion.service';
import { Edicion } from '../../../../../../main/webapp/app/entities/edicion/edicion.model';
import { FestivalService } from '../../../../../../main/webapp/app/entities/festival';

describe('Component Tests', () => {

    describe('Edicion Management Dialog Component', () => {
        let comp: EdicionDialogComponent;
        let fixture: ComponentFixture<EdicionDialogComponent>;
        let service: EdicionService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [BackstageTestModule],
                declarations: [EdicionDialogComponent],
                providers: [
                    FestivalService,
                    EdicionService
                ]
            })
            .overrideTemplate(EdicionDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(EdicionDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(EdicionService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new Edicion(123);
                        spyOn(service, 'update').and.returnValue(Observable.of(entity));
                        comp.edicion = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.update).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'edicionListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );

            it('Should call create service on save for new entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new Edicion();
                        spyOn(service, 'create').and.returnValue(Observable.of(entity));
                        comp.edicion = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.create).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'edicionListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
