/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { BackstageTestModule } from '../../../test.module';
import { EdicionDeleteDialogComponent } from '../../../../../../main/webapp/app/entities/edicion/edicion-delete-dialog.component';
import { EdicionService } from '../../../../../../main/webapp/app/entities/edicion/edicion.service';

describe('Component Tests', () => {

    describe('Edicion Management Delete Component', () => {
        let comp: EdicionDeleteDialogComponent;
        let fixture: ComponentFixture<EdicionDeleteDialogComponent>;
        let service: EdicionService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [BackstageTestModule],
                declarations: [EdicionDeleteDialogComponent],
                providers: [
                    EdicionService
                ]
            })
            .overrideTemplate(EdicionDeleteDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(EdicionDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(EdicionService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(Observable.of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
