/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';

import { BackstageTestModule } from '../../../test.module';
import { EdicionDetailComponent } from '../../../../../../main/webapp/app/entities/edicion/edicion-detail.component';
import { EdicionService } from '../../../../../../main/webapp/app/entities/edicion/edicion.service';
import { Edicion } from '../../../../../../main/webapp/app/entities/edicion/edicion.model';

describe('Component Tests', () => {

    describe('Edicion Management Detail Component', () => {
        let comp: EdicionDetailComponent;
        let fixture: ComponentFixture<EdicionDetailComponent>;
        let service: EdicionService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [BackstageTestModule],
                declarations: [EdicionDetailComponent],
                providers: [
                    EdicionService
                ]
            })
            .overrideTemplate(EdicionDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(EdicionDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(EdicionService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new Edicion(123)));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.edicion).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
