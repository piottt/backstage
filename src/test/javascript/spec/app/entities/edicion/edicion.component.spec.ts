/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { Headers } from '@angular/http';

import { BackstageTestModule } from '../../../test.module';
import { EdicionComponent } from '../../../../../../main/webapp/app/entities/edicion/edicion.component';
import { EdicionService } from '../../../../../../main/webapp/app/entities/edicion/edicion.service';
import { Edicion } from '../../../../../../main/webapp/app/entities/edicion/edicion.model';

describe('Component Tests', () => {

    describe('Edicion Management Component', () => {
        let comp: EdicionComponent;
        let fixture: ComponentFixture<EdicionComponent>;
        let service: EdicionService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [BackstageTestModule],
                declarations: [EdicionComponent],
                providers: [
                    EdicionService
                ]
            })
            .overrideTemplate(EdicionComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(EdicionComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(EdicionService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new Headers();
                headers.append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of({
                    json: [new Edicion(123)],
                    headers
                }));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.edicions[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
