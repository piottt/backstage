import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';
import * as path from 'path';
describe('Edicion e2e test', () => {

    let navBarPage: NavBarPage;
    let edicionDialogPage: EdicionDialogPage;
    let edicionComponentsPage: EdicionComponentsPage;
    const fileToUpload = '../../../../main/webapp/content/images/logo-jhipster.png';
    const absolutePath = path.resolve(__dirname, fileToUpload);

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Edicions', () => {
        navBarPage.goToEntity('edicion');
        edicionComponentsPage = new EdicionComponentsPage();
        expect(edicionComponentsPage.getTitle())
            .toMatch(/backstageApp.edicion.home.title/);

    });

    it('should load create Edicion dialog', () => {
        edicionComponentsPage.clickOnCreateButton();
        edicionDialogPage = new EdicionDialogPage();
        expect(edicionDialogPage.getModalTitle())
            .toMatch(/backstageApp.edicion.home.createOrEditLabel/);
        edicionDialogPage.close();
    });

    it('should create and save Edicions', () => {
        edicionComponentsPage.clickOnCreateButton();
        edicionDialogPage.setFechaIinicioInput('2000-12-31');
        expect(edicionDialogPage.getFechaIinicioInput()).toMatch('2000-12-31');
        edicionDialogPage.setFechaFinInput('2000-12-31');
        expect(edicionDialogPage.getFechaFinInput()).toMatch('2000-12-31');
        edicionDialogPage.setLogoInput(absolutePath);
        edicionDialogPage.festivalSelectLastOption();
        edicionDialogPage.save();
        expect(edicionDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class EdicionComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-edicion div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class EdicionDialogPage {
    modalTitle = element(by.css('h4#myEdicionLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    fechaIinicioInput = element(by.css('input#field_fechaIinicio'));
    fechaFinInput = element(by.css('input#field_fechaFin'));
    logoInput = element(by.css('input#file_logo'));
    festivalSelect = element(by.css('select#field_festival'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setFechaIinicioInput = function(fechaIinicio) {
        this.fechaIinicioInput.sendKeys(fechaIinicio);
    }

    getFechaIinicioInput = function() {
        return this.fechaIinicioInput.getAttribute('value');
    }

    setFechaFinInput = function(fechaFin) {
        this.fechaFinInput.sendKeys(fechaFin);
    }

    getFechaFinInput = function() {
        return this.fechaFinInput.getAttribute('value');
    }

    setLogoInput = function(logo) {
        this.logoInput.sendKeys(logo);
    }

    getLogoInput = function() {
        return this.logoInput.getAttribute('value');
    }

    festivalSelectLastOption = function() {
        this.festivalSelect.all(by.tagName('option')).last().click();
    }

    festivalSelectOption = function(option) {
        this.festivalSelect.sendKeys(option);
    }

    getFestivalSelect = function() {
        return this.festivalSelect;
    }

    getFestivalSelectedOption = function() {
        return this.festivalSelect.element(by.css('option:checked')).getText();
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
