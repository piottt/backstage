import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('Festival e2e test', () => {

    let navBarPage: NavBarPage;
    let festivalDialogPage: FestivalDialogPage;
    let festivalComponentsPage: FestivalComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Festivals', () => {
        navBarPage.goToEntity('festival');
        festivalComponentsPage = new FestivalComponentsPage();
        expect(festivalComponentsPage.getTitle())
            .toMatch(/backstageApp.festival.home.title/);

    });

    it('should load create Festival dialog', () => {
        festivalComponentsPage.clickOnCreateButton();
        festivalDialogPage = new FestivalDialogPage();
        expect(festivalDialogPage.getModalTitle())
            .toMatch(/backstageApp.festival.home.createOrEditLabel/);
        festivalDialogPage.close();
    });

    it('should create and save Festivals', () => {
        festivalComponentsPage.clickOnCreateButton();
        festivalDialogPage.setNombreInput('nombre');
        expect(festivalDialogPage.getNombreInput()).toMatch('nombre');
        festivalDialogPage.setPaisInput('pais');
        expect(festivalDialogPage.getPaisInput()).toMatch('pais');
        festivalDialogPage.save();
        expect(festivalDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class FestivalComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-festival div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class FestivalDialogPage {
    modalTitle = element(by.css('h4#myFestivalLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    nombreInput = element(by.css('input#field_nombre'));
    paisInput = element(by.css('input#field_pais'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setNombreInput = function(nombre) {
        this.nombreInput.sendKeys(nombre);
    }

    getNombreInput = function() {
        return this.nombreInput.getAttribute('value');
    }

    setPaisInput = function(pais) {
        this.paisInput.sendKeys(pais);
    }

    getPaisInput = function() {
        return this.paisInput.getAttribute('value');
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
