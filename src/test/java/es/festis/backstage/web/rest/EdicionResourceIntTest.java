package es.festis.backstage.web.rest;

import es.festis.backstage.BackstageApp;

import es.festis.backstage.domain.Edicion;
import es.festis.backstage.repository.EdicionRepository;
import es.festis.backstage.service.EdicionService;
import es.festis.backstage.service.dto.EdicionDTO;
import es.festis.backstage.service.mapper.EdicionMapper;
import es.festis.backstage.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static es.festis.backstage.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the EdicionResource REST controller.
 *
 * @see EdicionResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = BackstageApp.class)
public class EdicionResourceIntTest {

    private static final LocalDate DEFAULT_FECHA_IINICIO = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_FECHA_IINICIO = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_FECHA_FIN = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_FECHA_FIN = LocalDate.now(ZoneId.systemDefault());

    private static final byte[] DEFAULT_LOGO = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_LOGO = TestUtil.createByteArray(2, "1");
    private static final String DEFAULT_LOGO_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_LOGO_CONTENT_TYPE = "image/png";

    @Autowired
    private EdicionRepository edicionRepository;

    @Autowired
    private EdicionMapper edicionMapper;

    @Autowired
    private EdicionService edicionService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restEdicionMockMvc;

    private Edicion edicion;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final EdicionResource edicionResource = new EdicionResource(edicionService);
        this.restEdicionMockMvc = MockMvcBuilders.standaloneSetup(edicionResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Edicion createEntity(EntityManager em) {
        Edicion edicion = new Edicion()
            .fechaIinicio(DEFAULT_FECHA_IINICIO)
            .fechaFin(DEFAULT_FECHA_FIN)
            .logo(DEFAULT_LOGO)
            .logoContentType(DEFAULT_LOGO_CONTENT_TYPE);
        return edicion;
    }

    @Before
    public void initTest() {
        edicion = createEntity(em);
    }

    @Test
    @Transactional
    public void createEdicion() throws Exception {
        int databaseSizeBeforeCreate = edicionRepository.findAll().size();

        // Create the Edicion
        EdicionDTO edicionDTO = edicionMapper.toDto(edicion);
        restEdicionMockMvc.perform(post("/api/edicions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(edicionDTO)))
            .andExpect(status().isCreated());

        // Validate the Edicion in the database
        List<Edicion> edicionList = edicionRepository.findAll();
        assertThat(edicionList).hasSize(databaseSizeBeforeCreate + 1);
        Edicion testEdicion = edicionList.get(edicionList.size() - 1);
        assertThat(testEdicion.getFechaIinicio()).isEqualTo(DEFAULT_FECHA_IINICIO);
        assertThat(testEdicion.getFechaFin()).isEqualTo(DEFAULT_FECHA_FIN);
        assertThat(testEdicion.getLogo()).isEqualTo(DEFAULT_LOGO);
        assertThat(testEdicion.getLogoContentType()).isEqualTo(DEFAULT_LOGO_CONTENT_TYPE);
    }

    @Test
    @Transactional
    public void createEdicionWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = edicionRepository.findAll().size();

        // Create the Edicion with an existing ID
        edicion.setId(1L);
        EdicionDTO edicionDTO = edicionMapper.toDto(edicion);

        // An entity with an existing ID cannot be created, so this API call must fail
        restEdicionMockMvc.perform(post("/api/edicions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(edicionDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Edicion in the database
        List<Edicion> edicionList = edicionRepository.findAll();
        assertThat(edicionList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkFechaIinicioIsRequired() throws Exception {
        int databaseSizeBeforeTest = edicionRepository.findAll().size();
        // set the field null
        edicion.setFechaIinicio(null);

        // Create the Edicion, which fails.
        EdicionDTO edicionDTO = edicionMapper.toDto(edicion);

        restEdicionMockMvc.perform(post("/api/edicions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(edicionDTO)))
            .andExpect(status().isBadRequest());

        List<Edicion> edicionList = edicionRepository.findAll();
        assertThat(edicionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkFechaFinIsRequired() throws Exception {
        int databaseSizeBeforeTest = edicionRepository.findAll().size();
        // set the field null
        edicion.setFechaFin(null);

        // Create the Edicion, which fails.
        EdicionDTO edicionDTO = edicionMapper.toDto(edicion);

        restEdicionMockMvc.perform(post("/api/edicions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(edicionDTO)))
            .andExpect(status().isBadRequest());

        List<Edicion> edicionList = edicionRepository.findAll();
        assertThat(edicionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllEdicions() throws Exception {
        // Initialize the database
        edicionRepository.saveAndFlush(edicion);

        // Get all the edicionList
        restEdicionMockMvc.perform(get("/api/edicions?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(edicion.getId().intValue())))
            .andExpect(jsonPath("$.[*].fechaIinicio").value(hasItem(DEFAULT_FECHA_IINICIO.toString())))
            .andExpect(jsonPath("$.[*].fechaFin").value(hasItem(DEFAULT_FECHA_FIN.toString())))
            .andExpect(jsonPath("$.[*].logoContentType").value(hasItem(DEFAULT_LOGO_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].logo").value(hasItem(Base64Utils.encodeToString(DEFAULT_LOGO))));
    }

    @Test
    @Transactional
    public void getEdicion() throws Exception {
        // Initialize the database
        edicionRepository.saveAndFlush(edicion);

        // Get the edicion
        restEdicionMockMvc.perform(get("/api/edicions/{id}", edicion.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(edicion.getId().intValue()))
            .andExpect(jsonPath("$.fechaIinicio").value(DEFAULT_FECHA_IINICIO.toString()))
            .andExpect(jsonPath("$.fechaFin").value(DEFAULT_FECHA_FIN.toString()))
            .andExpect(jsonPath("$.logoContentType").value(DEFAULT_LOGO_CONTENT_TYPE))
            .andExpect(jsonPath("$.logo").value(Base64Utils.encodeToString(DEFAULT_LOGO)));
    }

    @Test
    @Transactional
    public void getNonExistingEdicion() throws Exception {
        // Get the edicion
        restEdicionMockMvc.perform(get("/api/edicions/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateEdicion() throws Exception {
        // Initialize the database
        edicionRepository.saveAndFlush(edicion);
        int databaseSizeBeforeUpdate = edicionRepository.findAll().size();

        // Update the edicion
        Edicion updatedEdicion = edicionRepository.findOne(edicion.getId());
        // Disconnect from session so that the updates on updatedEdicion are not directly saved in db
        em.detach(updatedEdicion);
        updatedEdicion
            .fechaIinicio(UPDATED_FECHA_IINICIO)
            .fechaFin(UPDATED_FECHA_FIN)
            .logo(UPDATED_LOGO)
            .logoContentType(UPDATED_LOGO_CONTENT_TYPE);
        EdicionDTO edicionDTO = edicionMapper.toDto(updatedEdicion);

        restEdicionMockMvc.perform(put("/api/edicions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(edicionDTO)))
            .andExpect(status().isOk());

        // Validate the Edicion in the database
        List<Edicion> edicionList = edicionRepository.findAll();
        assertThat(edicionList).hasSize(databaseSizeBeforeUpdate);
        Edicion testEdicion = edicionList.get(edicionList.size() - 1);
        assertThat(testEdicion.getFechaIinicio()).isEqualTo(UPDATED_FECHA_IINICIO);
        assertThat(testEdicion.getFechaFin()).isEqualTo(UPDATED_FECHA_FIN);
        assertThat(testEdicion.getLogo()).isEqualTo(UPDATED_LOGO);
        assertThat(testEdicion.getLogoContentType()).isEqualTo(UPDATED_LOGO_CONTENT_TYPE);
    }

    @Test
    @Transactional
    public void updateNonExistingEdicion() throws Exception {
        int databaseSizeBeforeUpdate = edicionRepository.findAll().size();

        // Create the Edicion
        EdicionDTO edicionDTO = edicionMapper.toDto(edicion);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restEdicionMockMvc.perform(put("/api/edicions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(edicionDTO)))
            .andExpect(status().isCreated());

        // Validate the Edicion in the database
        List<Edicion> edicionList = edicionRepository.findAll();
        assertThat(edicionList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteEdicion() throws Exception {
        // Initialize the database
        edicionRepository.saveAndFlush(edicion);
        int databaseSizeBeforeDelete = edicionRepository.findAll().size();

        // Get the edicion
        restEdicionMockMvc.perform(delete("/api/edicions/{id}", edicion.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Edicion> edicionList = edicionRepository.findAll();
        assertThat(edicionList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Edicion.class);
        Edicion edicion1 = new Edicion();
        edicion1.setId(1L);
        Edicion edicion2 = new Edicion();
        edicion2.setId(edicion1.getId());
        assertThat(edicion1).isEqualTo(edicion2);
        edicion2.setId(2L);
        assertThat(edicion1).isNotEqualTo(edicion2);
        edicion1.setId(null);
        assertThat(edicion1).isNotEqualTo(edicion2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(EdicionDTO.class);
        EdicionDTO edicionDTO1 = new EdicionDTO();
        edicionDTO1.setId(1L);
        EdicionDTO edicionDTO2 = new EdicionDTO();
        assertThat(edicionDTO1).isNotEqualTo(edicionDTO2);
        edicionDTO2.setId(edicionDTO1.getId());
        assertThat(edicionDTO1).isEqualTo(edicionDTO2);
        edicionDTO2.setId(2L);
        assertThat(edicionDTO1).isNotEqualTo(edicionDTO2);
        edicionDTO1.setId(null);
        assertThat(edicionDTO1).isNotEqualTo(edicionDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(edicionMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(edicionMapper.fromId(null)).isNull();
    }
}
