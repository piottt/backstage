package es.festis.backstage.web.rest;

import es.festis.backstage.BackstageApp;

import es.festis.backstage.domain.Festival;
import es.festis.backstage.repository.FestivalRepository;
import es.festis.backstage.service.FestivalService;
import es.festis.backstage.service.dto.FestivalDTO;
import es.festis.backstage.service.mapper.FestivalMapper;
import es.festis.backstage.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static es.festis.backstage.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the FestivalResource REST controller.
 *
 * @see FestivalResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = BackstageApp.class)
public class FestivalResourceIntTest {

    private static final String DEFAULT_NOMBRE = "AAAAAAAAAA";
    private static final String UPDATED_NOMBRE = "BBBBBBBBBB";

    private static final String DEFAULT_PAIS = "AAAAAAAAAA";
    private static final String UPDATED_PAIS = "BBBBBBBBBB";

    @Autowired
    private FestivalRepository festivalRepository;

    @Autowired
    private FestivalMapper festivalMapper;

    @Autowired
    private FestivalService festivalService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restFestivalMockMvc;

    private Festival festival;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final FestivalResource festivalResource = new FestivalResource(festivalService);
        this.restFestivalMockMvc = MockMvcBuilders.standaloneSetup(festivalResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Festival createEntity(EntityManager em) {
        Festival festival = new Festival()
            .nombre(DEFAULT_NOMBRE)
            .pais(DEFAULT_PAIS);
        return festival;
    }

    @Before
    public void initTest() {
        festival = createEntity(em);
    }

    @Test
    @Transactional
    public void createFestival() throws Exception {
        int databaseSizeBeforeCreate = festivalRepository.findAll().size();

        // Create the Festival
        FestivalDTO festivalDTO = festivalMapper.toDto(festival);
        restFestivalMockMvc.perform(post("/api/festivals")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(festivalDTO)))
            .andExpect(status().isCreated());

        // Validate the Festival in the database
        List<Festival> festivalList = festivalRepository.findAll();
        assertThat(festivalList).hasSize(databaseSizeBeforeCreate + 1);
        Festival testFestival = festivalList.get(festivalList.size() - 1);
        assertThat(testFestival.getNombre()).isEqualTo(DEFAULT_NOMBRE);
        assertThat(testFestival.getPais()).isEqualTo(DEFAULT_PAIS);
    }

    @Test
    @Transactional
    public void createFestivalWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = festivalRepository.findAll().size();

        // Create the Festival with an existing ID
        festival.setId(1L);
        FestivalDTO festivalDTO = festivalMapper.toDto(festival);

        // An entity with an existing ID cannot be created, so this API call must fail
        restFestivalMockMvc.perform(post("/api/festivals")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(festivalDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Festival in the database
        List<Festival> festivalList = festivalRepository.findAll();
        assertThat(festivalList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkNombreIsRequired() throws Exception {
        int databaseSizeBeforeTest = festivalRepository.findAll().size();
        // set the field null
        festival.setNombre(null);

        // Create the Festival, which fails.
        FestivalDTO festivalDTO = festivalMapper.toDto(festival);

        restFestivalMockMvc.perform(post("/api/festivals")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(festivalDTO)))
            .andExpect(status().isBadRequest());

        List<Festival> festivalList = festivalRepository.findAll();
        assertThat(festivalList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllFestivals() throws Exception {
        // Initialize the database
        festivalRepository.saveAndFlush(festival);

        // Get all the festivalList
        restFestivalMockMvc.perform(get("/api/festivals?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(festival.getId().intValue())))
            .andExpect(jsonPath("$.[*].nombre").value(hasItem(DEFAULT_NOMBRE.toString())))
            .andExpect(jsonPath("$.[*].pais").value(hasItem(DEFAULT_PAIS.toString())));
    }

    @Test
    @Transactional
    public void getFestival() throws Exception {
        // Initialize the database
        festivalRepository.saveAndFlush(festival);

        // Get the festival
        restFestivalMockMvc.perform(get("/api/festivals/{id}", festival.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(festival.getId().intValue()))
            .andExpect(jsonPath("$.nombre").value(DEFAULT_NOMBRE.toString()))
            .andExpect(jsonPath("$.pais").value(DEFAULT_PAIS.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingFestival() throws Exception {
        // Get the festival
        restFestivalMockMvc.perform(get("/api/festivals/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateFestival() throws Exception {
        // Initialize the database
        festivalRepository.saveAndFlush(festival);
        int databaseSizeBeforeUpdate = festivalRepository.findAll().size();

        // Update the festival
        Festival updatedFestival = festivalRepository.findOne(festival.getId());
        // Disconnect from session so that the updates on updatedFestival are not directly saved in db
        em.detach(updatedFestival);
        updatedFestival
            .nombre(UPDATED_NOMBRE)
            .pais(UPDATED_PAIS);
        FestivalDTO festivalDTO = festivalMapper.toDto(updatedFestival);

        restFestivalMockMvc.perform(put("/api/festivals")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(festivalDTO)))
            .andExpect(status().isOk());

        // Validate the Festival in the database
        List<Festival> festivalList = festivalRepository.findAll();
        assertThat(festivalList).hasSize(databaseSizeBeforeUpdate);
        Festival testFestival = festivalList.get(festivalList.size() - 1);
        assertThat(testFestival.getNombre()).isEqualTo(UPDATED_NOMBRE);
        assertThat(testFestival.getPais()).isEqualTo(UPDATED_PAIS);
    }

    @Test
    @Transactional
    public void updateNonExistingFestival() throws Exception {
        int databaseSizeBeforeUpdate = festivalRepository.findAll().size();

        // Create the Festival
        FestivalDTO festivalDTO = festivalMapper.toDto(festival);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restFestivalMockMvc.perform(put("/api/festivals")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(festivalDTO)))
            .andExpect(status().isCreated());

        // Validate the Festival in the database
        List<Festival> festivalList = festivalRepository.findAll();
        assertThat(festivalList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteFestival() throws Exception {
        // Initialize the database
        festivalRepository.saveAndFlush(festival);
        int databaseSizeBeforeDelete = festivalRepository.findAll().size();

        // Get the festival
        restFestivalMockMvc.perform(delete("/api/festivals/{id}", festival.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Festival> festivalList = festivalRepository.findAll();
        assertThat(festivalList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Festival.class);
        Festival festival1 = new Festival();
        festival1.setId(1L);
        Festival festival2 = new Festival();
        festival2.setId(festival1.getId());
        assertThat(festival1).isEqualTo(festival2);
        festival2.setId(2L);
        assertThat(festival1).isNotEqualTo(festival2);
        festival1.setId(null);
        assertThat(festival1).isNotEqualTo(festival2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(FestivalDTO.class);
        FestivalDTO festivalDTO1 = new FestivalDTO();
        festivalDTO1.setId(1L);
        FestivalDTO festivalDTO2 = new FestivalDTO();
        assertThat(festivalDTO1).isNotEqualTo(festivalDTO2);
        festivalDTO2.setId(festivalDTO1.getId());
        assertThat(festivalDTO1).isEqualTo(festivalDTO2);
        festivalDTO2.setId(2L);
        assertThat(festivalDTO1).isNotEqualTo(festivalDTO2);
        festivalDTO1.setId(null);
        assertThat(festivalDTO1).isNotEqualTo(festivalDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(festivalMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(festivalMapper.fromId(null)).isNull();
    }
}
