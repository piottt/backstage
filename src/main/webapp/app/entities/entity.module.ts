import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { BackstageFestivalModule } from './festival/festival.module';
import { BackstageEdicionModule } from './edicion/edicion.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    imports: [
        BackstageFestivalModule,
        BackstageEdicionModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BackstageEntityModule {}
