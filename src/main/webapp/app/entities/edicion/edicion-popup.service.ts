import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Edicion } from './edicion.model';
import { EdicionService } from './edicion.service';

@Injectable()
export class EdicionPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private edicionService: EdicionService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.edicionService.find(id).subscribe((edicion) => {
                    if (edicion.fechaIinicio) {
                        edicion.fechaIinicio = {
                            year: edicion.fechaIinicio.getFullYear(),
                            month: edicion.fechaIinicio.getMonth() + 1,
                            day: edicion.fechaIinicio.getDate()
                        };
                    }
                    if (edicion.fechaFin) {
                        edicion.fechaFin = {
                            year: edicion.fechaFin.getFullYear(),
                            month: edicion.fechaFin.getMonth() + 1,
                            day: edicion.fechaFin.getDate()
                        };
                    }
                    this.ngbModalRef = this.edicionModalRef(component, edicion);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.edicionModalRef(component, new Edicion());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    edicionModalRef(component: Component, edicion: Edicion): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.edicion = edicion;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
