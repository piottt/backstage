import { BaseEntity } from './../../shared';

export class Edicion implements BaseEntity {
    constructor(
        public id?: number,
        public fechaIinicio?: any,
        public fechaFin?: any,
        public logoContentType?: string,
        public logo?: any,
        public festivalId?: number,
    ) {
    }
}
