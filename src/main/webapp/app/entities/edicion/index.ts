export * from './edicion.model';
export * from './edicion-popup.service';
export * from './edicion.service';
export * from './edicion-dialog.component';
export * from './edicion-delete-dialog.component';
export * from './edicion-detail.component';
export * from './edicion.component';
export * from './edicion.route';
