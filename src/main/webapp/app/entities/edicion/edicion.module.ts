import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { BackstageSharedModule } from '../../shared';
import {
    EdicionService,
    EdicionPopupService,
    EdicionComponent,
    EdicionDetailComponent,
    EdicionDialogComponent,
    EdicionPopupComponent,
    EdicionDeletePopupComponent,
    EdicionDeleteDialogComponent,
    edicionRoute,
    edicionPopupRoute,
} from './';

const ENTITY_STATES = [
    ...edicionRoute,
    ...edicionPopupRoute,
];

@NgModule({
    imports: [
        BackstageSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        EdicionComponent,
        EdicionDetailComponent,
        EdicionDialogComponent,
        EdicionDeleteDialogComponent,
        EdicionPopupComponent,
        EdicionDeletePopupComponent,
    ],
    entryComponents: [
        EdicionComponent,
        EdicionDialogComponent,
        EdicionPopupComponent,
        EdicionDeleteDialogComponent,
        EdicionDeletePopupComponent,
    ],
    providers: [
        EdicionService,
        EdicionPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BackstageEdicionModule {}
