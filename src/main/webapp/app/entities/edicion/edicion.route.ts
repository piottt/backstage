import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { EdicionComponent } from './edicion.component';
import { EdicionDetailComponent } from './edicion-detail.component';
import { EdicionPopupComponent } from './edicion-dialog.component';
import { EdicionDeletePopupComponent } from './edicion-delete-dialog.component';

export const edicionRoute: Routes = [
    {
        path: 'edicion',
        component: EdicionComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'backstageApp.edicion.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'edicion/:id',
        component: EdicionDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'backstageApp.edicion.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const edicionPopupRoute: Routes = [
    {
        path: 'edicion-new',
        component: EdicionPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'backstageApp.edicion.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'edicion/:id/edit',
        component: EdicionPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'backstageApp.edicion.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'edicion/:id/delete',
        component: EdicionDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'backstageApp.edicion.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
