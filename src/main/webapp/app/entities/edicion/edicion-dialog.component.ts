import { Component, OnInit, OnDestroy, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService, JhiDataUtils } from 'ng-jhipster';

import { Edicion } from './edicion.model';
import { EdicionPopupService } from './edicion-popup.service';
import { EdicionService } from './edicion.service';
import { Festival, FestivalService } from '../festival';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-edicion-dialog',
    templateUrl: './edicion-dialog.component.html'
})
export class EdicionDialogComponent implements OnInit {

    edicion: Edicion;
    isSaving: boolean;

    festivals: Festival[];
    fechaIinicioDp: any;
    fechaFinDp: any;

    constructor(
        public activeModal: NgbActiveModal,
        private dataUtils: JhiDataUtils,
        private jhiAlertService: JhiAlertService,
        private edicionService: EdicionService,
        private festivalService: FestivalService,
        private elementRef: ElementRef,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.festivalService.query()
            .subscribe((res: ResponseWrapper) => { this.festivals = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }

    setFileData(event, entity, field, isImage) {
        this.dataUtils.setFileData(event, entity, field, isImage);
    }

    clearInputImage(field: string, fieldContentType: string, idInput: string) {
        this.dataUtils.clearInputImage(this.edicion, this.elementRef, field, fieldContentType, idInput);
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.edicion.id !== undefined) {
            this.subscribeToSaveResponse(
                this.edicionService.update(this.edicion));
        } else {
            this.subscribeToSaveResponse(
                this.edicionService.create(this.edicion));
        }
    }

    private subscribeToSaveResponse(result: Observable<Edicion>) {
        result.subscribe((res: Edicion) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: Edicion) {
        this.eventManager.broadcast({ name: 'edicionListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackFestivalById(index: number, item: Festival) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-edicion-popup',
    template: ''
})
export class EdicionPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private edicionPopupService: EdicionPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.edicionPopupService
                    .open(EdicionDialogComponent as Component, params['id']);
            } else {
                this.edicionPopupService
                    .open(EdicionDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
