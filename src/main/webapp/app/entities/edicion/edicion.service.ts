import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { Edicion } from './edicion.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class EdicionService {

    private resourceUrl =  SERVER_API_URL + 'api/edicions';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    create(edicion: Edicion): Observable<Edicion> {
        const copy = this.convert(edicion);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(edicion: Edicion): Observable<Edicion> {
        const copy = this.convert(edicion);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<Edicion> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to Edicion.
     */
    private convertItemFromServer(json: any): Edicion {
        const entity: Edicion = Object.assign(new Edicion(), json);
        entity.fechaIinicio = this.dateUtils
            .convertLocalDateFromServer(json.fechaIinicio);
        entity.fechaFin = this.dateUtils
            .convertLocalDateFromServer(json.fechaFin);
        return entity;
    }

    /**
     * Convert a Edicion to a JSON which can be sent to the server.
     */
    private convert(edicion: Edicion): Edicion {
        const copy: Edicion = Object.assign({}, edicion);
        copy.fechaIinicio = this.dateUtils
            .convertLocalDateToServer(edicion.fechaIinicio);
        copy.fechaFin = this.dateUtils
            .convertLocalDateToServer(edicion.fechaFin);
        return copy;
    }
}
