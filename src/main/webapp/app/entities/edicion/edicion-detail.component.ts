import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiDataUtils } from 'ng-jhipster';

import { Edicion } from './edicion.model';
import { EdicionService } from './edicion.service';

@Component({
    selector: 'jhi-edicion-detail',
    templateUrl: './edicion-detail.component.html'
})
export class EdicionDetailComponent implements OnInit, OnDestroy {

    edicion: Edicion;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private dataUtils: JhiDataUtils,
        private edicionService: EdicionService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInEdicions();
    }

    load(id) {
        this.edicionService.find(id).subscribe((edicion) => {
            this.edicion = edicion;
        });
    }
    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInEdicions() {
        this.eventSubscriber = this.eventManager.subscribe(
            'edicionListModification',
            (response) => this.load(this.edicion.id)
        );
    }
}
