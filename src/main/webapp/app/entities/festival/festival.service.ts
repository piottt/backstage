import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { Festival } from './festival.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class FestivalService {

    private resourceUrl =  SERVER_API_URL + 'api/festivals';

    constructor(private http: Http) { }

    create(festival: Festival): Observable<Festival> {
        const copy = this.convert(festival);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(festival: Festival): Observable<Festival> {
        const copy = this.convert(festival);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<Festival> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to Festival.
     */
    private convertItemFromServer(json: any): Festival {
        const entity: Festival = Object.assign(new Festival(), json);
        return entity;
    }

    /**
     * Convert a Festival to a JSON which can be sent to the server.
     */
    private convert(festival: Festival): Festival {
        const copy: Festival = Object.assign({}, festival);
        return copy;
    }
}
