import { BaseEntity } from './../../shared';

export class Festival implements BaseEntity {
    constructor(
        public id?: number,
        public nombre?: string,
        public pais?: string,
        public ediciones?: BaseEntity[],
    ) {
    }
}
