package es.festis.backstage.service;

import es.festis.backstage.domain.Edicion;
import es.festis.backstage.repository.EdicionRepository;
import es.festis.backstage.service.dto.EdicionDTO;
import es.festis.backstage.service.mapper.EdicionMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing Edicion.
 */
@Service
@Transactional
public class EdicionService {

    private final Logger log = LoggerFactory.getLogger(EdicionService.class);

    private final EdicionRepository edicionRepository;

    private final EdicionMapper edicionMapper;

    public EdicionService(EdicionRepository edicionRepository, EdicionMapper edicionMapper) {
        this.edicionRepository = edicionRepository;
        this.edicionMapper = edicionMapper;
    }

    /**
     * Save a edicion.
     *
     * @param edicionDTO the entity to save
     * @return the persisted entity
     */
    public EdicionDTO save(EdicionDTO edicionDTO) {
        log.debug("Request to save Edicion : {}", edicionDTO);
        Edicion edicion = edicionMapper.toEntity(edicionDTO);
        edicion = edicionRepository.save(edicion);
        return edicionMapper.toDto(edicion);
    }

    /**
     * Get all the edicions.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<EdicionDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Edicions");
        return edicionRepository.findAll(pageable)
            .map(edicionMapper::toDto);
    }

    /**
     * Get one edicion by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public EdicionDTO findOne(Long id) {
        log.debug("Request to get Edicion : {}", id);
        Edicion edicion = edicionRepository.findOne(id);
        return edicionMapper.toDto(edicion);
    }

    /**
     * Delete the edicion by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Edicion : {}", id);
        edicionRepository.delete(id);
    }
}
