package es.festis.backstage.service.dto;


import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import javax.persistence.Lob;

/**
 * A DTO for the Edicion entity.
 */
public class EdicionDTO implements Serializable {

    private Long id;

    @NotNull
    private LocalDate fechaIinicio;

    @NotNull
    private LocalDate fechaFin;

    @Lob
    private byte[] logo;
    private String logoContentType;

    private Long festivalId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getFechaIinicio() {
        return fechaIinicio;
    }

    public void setFechaIinicio(LocalDate fechaIinicio) {
        this.fechaIinicio = fechaIinicio;
    }

    public LocalDate getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(LocalDate fechaFin) {
        this.fechaFin = fechaFin;
    }

    public byte[] getLogo() {
        return logo;
    }

    public void setLogo(byte[] logo) {
        this.logo = logo;
    }

    public String getLogoContentType() {
        return logoContentType;
    }

    public void setLogoContentType(String logoContentType) {
        this.logoContentType = logoContentType;
    }

    public Long getFestivalId() {
        return festivalId;
    }

    public void setFestivalId(Long festivalId) {
        this.festivalId = festivalId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        EdicionDTO edicionDTO = (EdicionDTO) o;
        if(edicionDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), edicionDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "EdicionDTO{" +
            "id=" + getId() +
            ", fechaIinicio='" + getFechaIinicio() + "'" +
            ", fechaFin='" + getFechaFin() + "'" +
            ", logo='" + getLogo() + "'" +
            "}";
    }
}
