package es.festis.backstage.service;

import es.festis.backstage.domain.Festival;
import es.festis.backstage.repository.FestivalRepository;
import es.festis.backstage.service.dto.FestivalDTO;
import es.festis.backstage.service.mapper.FestivalMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing Festival.
 */
@Service
@Transactional
public class FestivalService {

    private final Logger log = LoggerFactory.getLogger(FestivalService.class);

    private final FestivalRepository festivalRepository;

    private final FestivalMapper festivalMapper;

    public FestivalService(FestivalRepository festivalRepository, FestivalMapper festivalMapper) {
        this.festivalRepository = festivalRepository;
        this.festivalMapper = festivalMapper;
    }

    /**
     * Save a festival.
     *
     * @param festivalDTO the entity to save
     * @return the persisted entity
     */
    public FestivalDTO save(FestivalDTO festivalDTO) {
        log.debug("Request to save Festival : {}", festivalDTO);
        Festival festival = festivalMapper.toEntity(festivalDTO);
        festival = festivalRepository.save(festival);
        return festivalMapper.toDto(festival);
    }

    /**
     * Get all the festivals.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<FestivalDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Festivals");
        return festivalRepository.findAll(pageable)
            .map(festivalMapper::toDto);
    }

    /**
     * Get one festival by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public FestivalDTO findOne(Long id) {
        log.debug("Request to get Festival : {}", id);
        Festival festival = festivalRepository.findOne(id);
        return festivalMapper.toDto(festival);
    }

    /**
     * Delete the festival by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Festival : {}", id);
        festivalRepository.delete(id);
    }
}
