package es.festis.backstage.service.mapper;

import es.festis.backstage.domain.*;
import es.festis.backstage.service.dto.EdicionDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Edicion and its DTO EdicionDTO.
 */
@Mapper(componentModel = "spring", uses = {FestivalMapper.class})
public interface EdicionMapper extends EntityMapper<EdicionDTO, Edicion> {

    @Mapping(source = "festival.id", target = "festivalId")
    EdicionDTO toDto(Edicion edicion);

    @Mapping(source = "festivalId", target = "festival")
    Edicion toEntity(EdicionDTO edicionDTO);

    default Edicion fromId(Long id) {
        if (id == null) {
            return null;
        }
        Edicion edicion = new Edicion();
        edicion.setId(id);
        return edicion;
    }
}
