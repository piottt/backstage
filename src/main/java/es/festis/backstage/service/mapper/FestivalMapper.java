package es.festis.backstage.service.mapper;

import es.festis.backstage.domain.*;
import es.festis.backstage.service.dto.FestivalDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Festival and its DTO FestivalDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface FestivalMapper extends EntityMapper<FestivalDTO, Festival> {


    @Mapping(target = "ediciones", ignore = true)
    Festival toEntity(FestivalDTO festivalDTO);

    default Festival fromId(Long id) {
        if (id == null) {
            return null;
        }
        Festival festival = new Festival();
        festival.setId(id);
        return festival;
    }
}
