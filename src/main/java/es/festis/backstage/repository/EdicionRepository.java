package es.festis.backstage.repository;

import es.festis.backstage.domain.Edicion;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Edicion entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EdicionRepository extends JpaRepository<Edicion, Long> {

}
