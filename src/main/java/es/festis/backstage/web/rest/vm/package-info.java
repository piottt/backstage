/**
 * View Models used by Spring MVC REST controllers.
 */
package es.festis.backstage.web.rest.vm;
