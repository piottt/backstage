package es.festis.backstage.web.rest;

import com.codahale.metrics.annotation.Timed;
import es.festis.backstage.service.EdicionService;
import es.festis.backstage.web.rest.errors.BadRequestAlertException;
import es.festis.backstage.web.rest.util.HeaderUtil;
import es.festis.backstage.web.rest.util.PaginationUtil;
import es.festis.backstage.service.dto.EdicionDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Edicion.
 */
@RestController
@RequestMapping("/api")
public class EdicionResource {

    private final Logger log = LoggerFactory.getLogger(EdicionResource.class);

    private static final String ENTITY_NAME = "edicion";

    private final EdicionService edicionService;

    public EdicionResource(EdicionService edicionService) {
        this.edicionService = edicionService;
    }

    /**
     * POST  /edicions : Create a new edicion.
     *
     * @param edicionDTO the edicionDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new edicionDTO, or with status 400 (Bad Request) if the edicion has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/edicions")
    @Timed
    public ResponseEntity<EdicionDTO> createEdicion(@Valid @RequestBody EdicionDTO edicionDTO) throws URISyntaxException {
        log.debug("REST request to save Edicion : {}", edicionDTO);
        if (edicionDTO.getId() != null) {
            throw new BadRequestAlertException("A new edicion cannot already have an ID", ENTITY_NAME, "idexists");
        }
        EdicionDTO result = edicionService.save(edicionDTO);
        return ResponseEntity.created(new URI("/api/edicions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /edicions : Updates an existing edicion.
     *
     * @param edicionDTO the edicionDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated edicionDTO,
     * or with status 400 (Bad Request) if the edicionDTO is not valid,
     * or with status 500 (Internal Server Error) if the edicionDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/edicions")
    @Timed
    public ResponseEntity<EdicionDTO> updateEdicion(@Valid @RequestBody EdicionDTO edicionDTO) throws URISyntaxException {
        log.debug("REST request to update Edicion : {}", edicionDTO);
        if (edicionDTO.getId() == null) {
            return createEdicion(edicionDTO);
        }
        EdicionDTO result = edicionService.save(edicionDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, edicionDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /edicions : get all the edicions.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of edicions in body
     */
    @GetMapping("/edicions")
    @Timed
    public ResponseEntity<List<EdicionDTO>> getAllEdicions(Pageable pageable) {
        log.debug("REST request to get a page of Edicions");
        Page<EdicionDTO> page = edicionService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/edicions");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /edicions/:id : get the "id" edicion.
     *
     * @param id the id of the edicionDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the edicionDTO, or with status 404 (Not Found)
     */
    @GetMapping("/edicions/{id}")
    @Timed
    public ResponseEntity<EdicionDTO> getEdicion(@PathVariable Long id) {
        log.debug("REST request to get Edicion : {}", id);
        EdicionDTO edicionDTO = edicionService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(edicionDTO));
    }

    /**
     * DELETE  /edicions/:id : delete the "id" edicion.
     *
     * @param id the id of the edicionDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/edicions/{id}")
    @Timed
    public ResponseEntity<Void> deleteEdicion(@PathVariable Long id) {
        log.debug("REST request to delete Edicion : {}", id);
        edicionService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
