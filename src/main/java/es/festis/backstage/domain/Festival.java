package es.festis.backstage.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Festival.
 */
@Entity
@Table(name = "festival")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Festival implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "nombre", nullable = false)
    private String nombre;

    @Column(name = "pais")
    private String pais;

    @OneToMany(mappedBy = "festival")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Edicion> ediciones = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public Festival nombre(String nombre) {
        this.nombre = nombre;
        return this;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPais() {
        return pais;
    }

    public Festival pais(String pais) {
        this.pais = pais;
        return this;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public Set<Edicion> getEdiciones() {
        return ediciones;
    }

    public Festival ediciones(Set<Edicion> edicions) {
        this.ediciones = edicions;
        return this;
    }

    public Festival addEdiciones(Edicion edicion) {
        this.ediciones.add(edicion);
        edicion.setFestival(this);
        return this;
    }

    public Festival removeEdiciones(Edicion edicion) {
        this.ediciones.remove(edicion);
        edicion.setFestival(null);
        return this;
    }

    public void setEdiciones(Set<Edicion> edicions) {
        this.ediciones = edicions;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Festival festival = (Festival) o;
        if (festival.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), festival.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Festival{" +
            "id=" + getId() +
            ", nombre='" + getNombre() + "'" +
            ", pais='" + getPais() + "'" +
            "}";
    }
}
