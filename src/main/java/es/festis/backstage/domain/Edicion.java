package es.festis.backstage.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A Edicion.
 */
@Entity
@Table(name = "edicion")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Edicion implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "fecha_iinicio", nullable = false)
    private LocalDate fechaIinicio;

    @NotNull
    @Column(name = "fecha_fin", nullable = false)
    private LocalDate fechaFin;

    @Lob
    @Column(name = "logo")
    private byte[] logo;

    @Column(name = "logo_content_type")
    private String logoContentType;

    @ManyToOne
    private Festival festival;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getFechaIinicio() {
        return fechaIinicio;
    }

    public Edicion fechaIinicio(LocalDate fechaIinicio) {
        this.fechaIinicio = fechaIinicio;
        return this;
    }

    public void setFechaIinicio(LocalDate fechaIinicio) {
        this.fechaIinicio = fechaIinicio;
    }

    public LocalDate getFechaFin() {
        return fechaFin;
    }

    public Edicion fechaFin(LocalDate fechaFin) {
        this.fechaFin = fechaFin;
        return this;
    }

    public void setFechaFin(LocalDate fechaFin) {
        this.fechaFin = fechaFin;
    }

    public byte[] getLogo() {
        return logo;
    }

    public Edicion logo(byte[] logo) {
        this.logo = logo;
        return this;
    }

    public void setLogo(byte[] logo) {
        this.logo = logo;
    }

    public String getLogoContentType() {
        return logoContentType;
    }

    public Edicion logoContentType(String logoContentType) {
        this.logoContentType = logoContentType;
        return this;
    }

    public void setLogoContentType(String logoContentType) {
        this.logoContentType = logoContentType;
    }

    public Festival getFestival() {
        return festival;
    }

    public Edicion festival(Festival festival) {
        this.festival = festival;
        return this;
    }

    public void setFestival(Festival festival) {
        this.festival = festival;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Edicion edicion = (Edicion) o;
        if (edicion.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), edicion.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Edicion{" +
            "id=" + getId() +
            ", fechaIinicio='" + getFechaIinicio() + "'" +
            ", fechaFin='" + getFechaFin() + "'" +
            ", logo='" + getLogo() + "'" +
            ", logoContentType='" + getLogoContentType() + "'" +
            "}";
    }
}
